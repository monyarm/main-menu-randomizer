use rand::Rng;
use regex::Regex;
use serde::Deserialize;
use std::{
    ffi::c_void,
    io::{Read, Write},
    os::raw::c_char,
    path::{Path, PathBuf, StripPrefixError},
};
use thiserror::Error;
use const_format::formatcp;

#[derive(Debug, Deserialize, PartialEq, PartialOrd)]
enum LogLevel {
    Error = 1,
    Warn = 2,
    Info = 3,
    Debug = 4,
    Trace = 5,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
struct Config {
    source: Vec<Directory>,
    destination: PathBuf,
    filter: Filter,
    log_level: LogLevel,
    #[serde(default)]
    anniversary_workaroud: bool,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
struct Directory {
    directory: PathBuf,
    filter: Option<Filter>,
}

#[derive(Debug, Deserialize, Clone)]
#[serde(try_from = "String")]
struct Filter(Regex);

impl std::fmt::Display for Filter {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0.as_str())
    }
}

impl TryFrom<String> for Filter {
    type Error = regex::Error;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        let regex = Regex::new(&value)?;

        Ok(Self(regex))
    }
}

#[derive(Debug)]
#[repr(C)]
pub struct SKSEPluginVersionData {
    data_version: u32,
    plugin_version: u32,
    name: [u8; 256],
    author: [u8; 256],
    support_email: [u8; 256],
    version_independence: u32,
    compatible_versions: [u32; 16],
    se_version_required: u32,
}

const fn zero_pad_u8<const N: usize, const M: usize>(arr: &[u8; N]) -> [u8; M] {
    let mut m = [0; M];
    let mut i = 0;
    while i < N {
        m[i] = arr[i];
        i += 1;
    }
    m
}

static NAME: &[u8; 21] = b"MainMenuRandomizerSE\0";
static AUTHOR: &[u8; 10] = b"Lanastara\0";
static EMAIL: &[u8; 24] = b"fritz.mannhal@gmail.com\0";

#[no_mangle]
pub static SKSEPlugin_Version: SKSEPluginVersionData = SKSEPluginVersionData {
    data_version: 1,
    plugin_version: 4,
    name: zero_pad_u8(NAME),
    author: zero_pad_u8(AUTHOR),
    support_email: zero_pad_u8(EMAIL),
    version_independence: 1 << 1,
    compatible_versions: [0; 16],
    se_version_required: 0,
};

#[derive(Debug, Error)]
pub enum Error {
    #[error("IOError: {0:?}")]
    IoError(#[from] std::io::Error),
    #[error("Replacer does not contain a Data Directory")]
    DataMissing,
    #[error("SourceDirectory does not exist")]
    SourceMissing,
    #[error("SourceDirectory is empty")]
    SourceEmpty,
    #[error("Could not read Config File")]
    ConfigError,

    #[error("Could not evaluate relative path")]
    StripPrefixError(#[from] StripPrefixError),
}

struct Logger {
    log_level: LogLevel,
    file: std::fs::File,
}

impl Logger {
    fn log<TMessage: AsRef<str>>(
        &mut self,
        level: LogLevel,
        message: TMessage,
    ) -> Result<(), std::io::Error> {
        if level <= self.log_level {
            writeln!(&self.file, "{}", message.as_ref())?;

            self.file.flush()?
        }

        Ok(())
    }

    fn error<TMessage: AsRef<str>>(&mut self, message: TMessage) -> Result<(), std::io::Error> {
        self.log(LogLevel::Error, message)
    }

    fn _warn<TMessage: AsRef<str>>(&mut self, message: TMessage) -> Result<(), std::io::Error> {
        self.log(LogLevel::Warn, message)
    }

    fn info<TMessage: AsRef<str>>(&mut self, message: TMessage) -> Result<(), std::io::Error> {
        self.log(LogLevel::Info, message)
    }

    fn debug<TMessage: AsRef<str>>(&mut self, message: TMessage) -> Result<(), std::io::Error> {
        self.log(LogLevel::Debug, message)
    }

    fn trace<TMessage: AsRef<str>>(&mut self, message: TMessage) -> Result<(), std::io::Error> {
        self.log(LogLevel::Trace, message)
    }
}

impl Drop for Logger {
    fn drop(&mut self) {
        let _ = self.file.flush();
    }
}

fn handle_dir(
    directory: Directory,
    filter: Filter,
    logger: &mut Logger,
    destination: &PathBuf,
    ae_workaround: bool,
) -> Result<(), Error> {
    let filter = directory.filter.unwrap_or(filter);
    logger.debug(format!(
        "Using Source directory: '{:?}' with filter '{}'",
        directory.directory, filter
    ))?;
    if !directory.directory.exists() {
        return Err(Error::SourceMissing);
    }

    let read_dir = directory.directory.read_dir()?;
    let directories: Vec<_> = read_dir
        .filter_map(|r| r.ok())
        .filter(|dir| dir.metadata().map(|m| m.is_dir()).unwrap_or_default())
        .collect();

    logger.debug(format!(
        "Found the following possible replacers: {:?}",
        directories
    ))?;

    if directories.len() == 0 {
        return Err(Error::SourceEmpty);
    }

    let index = rand::thread_rng().gen_range(0..directories.len());
    let dir = &directories[index].path();

    logger.info(format!("Using Replacer at: '{:?}'", dir))?;

    let data = dir.join("data");

    if !data.exists() {
        return Err(Error::DataMissing);
    }

    logger.debug(format!("Copying files to: '{:?}'", destination))?;

    let walkdir = walkdir::WalkDir::new(&data);

    let mut logo_path = Path::new("meshes").to_path_buf();
    logo_path.push("interface");
    logo_path.push("logo");

    let mut logo2_path = logo_path.clone();
    logo2_path.push("logo01ae.nif");
    logo_path.push("logo.nif");

    for d in walkdir {
        if let Ok(d) = d {
            let abs_src = d.path();
            if abs_src.is_file() {
                let rel_src = abs_src.strip_prefix(&data)?;

                if let Some(rel_src_str) = rel_src.to_str() {
                    if filter.0.is_match(rel_src_str) {
                        let abs_dst = destination.join(&rel_src);

                        let dst_dir = abs_dst.parent();

                        if let Some(dst_dir) = dst_dir {
                            if !abs_dst.exists() {
                                logger
                                    .debug(format!("Creating missing Directory: '{:?}", dst_dir))?;
                                std::fs::create_dir_all(dst_dir)?;
                            }
                        }

                        logger.trace(format!("Copying file: '{:?}'", rel_src))?;

                        std::fs::copy(abs_src, abs_dst)?;

                        if ae_workaround && rel_src == logo_path {
                            let abs_dst = destination.join(&logo2_path);
                            logger.trace(format!("Copying file to '{:?}'", abs_dst))?;
                            std::fs::copy(abs_src, abs_dst)?;
                        }
                    } else {
                        logger.info(format!("File '{:?}' skipped due to filter.", rel_src))?;
                    }
                }
            }
        } else {
            logger.error(format!("Invalid Directory Entry encountered"))?;
        }
    }

    Ok(())
}

pub fn replace() -> Result<(), Error> {
    let mut log_file = std::fs::File::create(formatcp!(".\\data\\{xSE}\\plugins\\MainMenuRandomizer.log", xSE = env!("xSE")))?;

    log_file.write_fmt(format_args!(
        "MainMenuRandomizer: Starting Version {}\n",
        std::env!("CARGO_PKG_VERSION")
    ))?;
    log_file.flush()?;

    let mut config_file_buffer = Vec::new();
    std::fs::File::open(formatcp!(".\\data\\{xSE}\\plugins\\MainMenuRandomizer.config.toml", xSE = env!("xSE")))?
        .read_to_end(&mut config_file_buffer)?;

    let config: Config = match toml::from_slice(&config_file_buffer) {
        Ok(c) => c,
        Err(e) => {
            log_file.write_fmt(format_args!("Error in Configfile: '{:?}'\n", e))?;
            log_file.flush()?;
            return Err(Error::ConfigError);
        }
    };
    let ae_workaroud = config.anniversary_workaroud;

    let mut logger = Logger {
        file: log_file,
        log_level: config.log_level,
    };

    logger.trace("Config Loaded")?;

    if !config.destination.exists() {
        logger.error("Destination Directory does not exist")?;
    }

    for directory in config.source {
        if let Err(e) = handle_dir(
            directory,
            config.filter.clone(),
            &mut logger,
            &config.destination,
            ae_workaroud,
        ) {
            logger.error(format!("{}", e))?;
        }
    }

    Ok(())
}

#[repr(C)]
pub struct PluginInfo {
    info_version: u32,
    name: *const c_char,
    version: u32,
}

#[no_mangle]
pub extern "C" fn SKSEPlugin_Query(_skse: *const c_void, info: *mut PluginInfo) -> bool {
    let mut info = unsafe { &mut *info };

    info.info_version = 1;
    info.name = NAME.as_ptr() as *const c_char;
    info.version = 1;
    true
}

#[no_mangle]
pub extern "C" fn SKSEPlugin_Load(_skse: *const c_void) -> bool {
    replace().is_ok()
}

#[no_mangle]
pub extern "C" fn F4SEPlugin_Query(_skse: *const c_void, info: *mut PluginInfo) -> bool {
    let mut info = unsafe { &mut *info };

    info.info_version = 1;
    info.name = NAME.as_ptr() as *const c_char;
    info.version = 1;
    true
}

#[no_mangle]
pub extern "C" fn F4SEPlugin_Load(_skse: *const c_void) -> bool {
    replace().is_ok()
}

#[no_mangle]
pub extern "C" fn NVSEPlugin_Query(_skse: *const c_void, info: *mut PluginInfo) -> bool {
    let mut info = unsafe { &mut *info };

    info.info_version = 1;
    info.name = NAME.as_ptr() as *const c_char;
    info.version = 1;
    true
}

#[no_mangle]
pub extern "C" fn NVSEPlugin_Load(_skse: *const c_void) -> bool {
    replace().is_ok()
}

#[no_mangle]
pub extern "C" fn OBSEPlugin_Query(_skse: *const c_void, info: *mut PluginInfo) -> bool {
    let mut info = unsafe { &mut *info };

    info.info_version = 1;
    info.name = NAME.as_ptr() as *const c_char;
    info.version = 1;
    true
}

#[no_mangle]
pub extern "C" fn OBSEPlugin_Load(_skse: *const c_void) -> bool {
    replace().is_ok()
}
